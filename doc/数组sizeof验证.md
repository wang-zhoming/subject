# 数组sizeof验证

### 一、介绍
{**以下是针对定义数组长度为sizeof(int)时所做的验证}

### 二、代码说明
定义数组，使数组的长度为sizeof(int)或sizeof(long)，分别在Linux和Windows下编译和运行以进行验证。

### 三、验证过程

输出sizeof（int）和sizeof（long)的值。


### 四、验证结果

1.  编译时环境已经定下来了，sizeof（int）和sizeof（long）是个定值，能够用于定义数组长度；
2.  64位Windows环境下sizeof（long）值为4，64位Linux下sizeof（long）值为8。