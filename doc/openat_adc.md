[TOC]

# ADC接口功能

## 功能属性：

## 平台特性：

## 特性和性能的测试：

## 接口功能测试：

<!--注：接口功能的测试，确定各个参数的使用边界，测试用例需要覆盖到所有支持的参数-->

### 1.OPENAT_OpenAdc

- 函数功能

  ADC打开接口
  
- 函数定义

  BOOL OPENAT_OpenAdc(

   E_AMOPENAT_ADC_CHANNEL channel,

   E_AMOPENAT_ADC_SCALE scale

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  |  参数   |                             注释                             |
  | :-----: | :----------------------------------------------------------: |
  | channel |                   ADC通道  0(adc0),1(adc1)                   |
  |  scale  | ADC读取量程，提高读取精度<br>typedef enum E_AMOPENAT_ADC_SCALE_TAG<br/>{           OPENAT_ADC_SCALE_DEFAULT, //最大5v<br/>OPENAT_ADC_SCALE_1V250,//0--1.25v<br/>OPENAT_ADC_SCALE_2V444,//0--2.4v<br/>OPENAT_ADC_SCALE_3V233,0--3.2v <br/>OPENAT_ADC_SCALE_5V000,0--5v <br/>   OPENAT_ADC_SCALE_QTY,<br/>}E_AMOPENAT_ADC_SCALE; |


### 2.OPENAT_ReadAdc

- 函数功能

  ADC读接口

- 函数定义

  OPENAT_ReadAdc(

  E_AMOPENAT_ADC_CHANNEL chanle,  

  UINT16* adcValue,          

  UINT16* voltage)

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  |   参数   |           注释           |
  | :------: | :----------------------: |
  | channel  | ADC通道  0(adc0),1(adc1) |
  | adcValue |          ADC值           |
  | voltage  |          电压值          |

  注意：ADC原始测量数据的值是电压值（单位mv）的三分之一，例如电压值是4200mv，则ADC值为1400

### 3.OPENAT_CloseAdc

- 函数功能

  ADC关闭接口

- 函数定义

  OPENAT_CloseAdc(

   E_AMOPENAT_ADC_CHANNEL channel

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  |  参数   |           注释           |
  | :-----: | :----------------------: |
  | channel | ADC通道  0(adc0),1(adc1) |

## 测试用例：

--测试用例1：测试ADC0检测范围为OPENAT_ADC_SCALE_DEFAULT。

​         -步骤1：配置相关参数，设置ADC值为1400，电压值为4200。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC0。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC0。

​         -结果  ：三接口均返回TURE。

--测试用例2：测试ADC0检测范围为OPENAT_ADC_SCALE_1V250。

​         -步骤1：配置相关参数，设置ADC值为300，电压值为900。

​         -步骤2：调用打开ADC接口，打开ADC0。

​		 -步骤3：调用读ADC接口，将相关参数带入。

​         -步骤4：调用关闭ADC接口，关闭ADC0。

​         -结果  ：三接口均返回TURE。

--测试用例3：测试ADC0检测范围为OPENAT_ADC_SCALE_2V444。

​         -步骤1：配置相关参数，设置ADC值为600，电压值为1800。

​         -步骤2：从外部接入1.8V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC0。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC0。

​         -结果  ：三接口均返回TURE。

--测试用例4：测试ADC0检测范围为OPENAT_ADC_SCALE_3V233。

​         -步骤1：配置相关参数，设置ADC值为900，电压值为2700。

​         -步骤2：从外部接入3V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC0。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC0。

​         -结果  ：三接口均返回TURE。

--测试用例5：测试ADC0检测范围为OPENAT_ADC_SCALE_5V000。

​         -步骤1：配置相关参数，设置ADC值为1500，电压值为4500。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC0。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC0。

​         -结果   ：三接口均返回TURE。

--测试用例6：测试ADC1检测范围为OPENAT_ADC_SCALE_DEFAULT。

​         -步骤1：配置相关参数，设置ADC值为1400，电压值为4200。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果  ：三接口均返回TURE。

--测试用例7：测试ADC1检测范围为OPENAT_ADC_SCALE_1V250。

​         -步骤1：配置相关参数，设置ADC值为300，电压值为900。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果  ：三接口均返回TURE。

--测试用例8：测试ADC1检测范围为OPENAT_ADC_SCALE_2V444。

​         -步骤1：配置相关参数，设置ADC值为600，电压值为1800。

​         -步骤2：从外部接入1.8V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果  ：三接口均返回TURE。

--测试用例9：测试ADC1检测范围为OPENAT_ADC_SCALE_3V233。

​         -步骤1：配置相关参数，设置ADC值为900，电压值为2700。

​         -步骤2：从外部接入3V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果  ：三接口均返回TURE。

--测试用例10：测试ADC1检测范围为OPENAT_ADC_SCALE_5V000。

​         -步骤1：配置相关参数，设置ADC值为1500，电压值为4500。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果   ：三接口均返回TURE。
