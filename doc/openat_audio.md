[TOC]

# Audio接口功能

## 功能属性

<!--注:描述功能各个属性，和属性的边界范围-->

## 平台特性

<!--注：描述当前平台支持的特性-->

## 特性和性能的测试

<!--注：添加功能的特性和性能测试-->

## 接口功能测试

<!--注：接口功能的测试，确定各个参数的使用边界，测试用例需要覆盖到所有支持的参数-->

### 1. 音频播放

#### 1.1 OPENAT_PlayMusic

- 函数功能

  music播放接口

- 函数定义

  BOOL OPENAT_PlayMusic(

   T_AMOPENAT_PLAY_PARAM *param

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  | 参数  | <div align="center" valign="middle">注释</div>               |
  | :---: | :----------------------------------------------------------- |
  | param | typedef struct T_AMOPENAT_PLAY_PARAM_TAG{<br/>BOOL playBuffer;/*是播放buffer还是播放文件*/  <br/>union { <br/>T_AMOPENAT_PLAY_BUFFER_PARAM playBufferParam; //播放buff结构体<br/> T_AMOPENAT_PLAY_FILE_PARAM playFileParam; //播放文件结构<br/>}; <br/>}T_AMOPENAT_PLAY_PARAM; |



#### 1.2 OPENAT_StopMusic

- 函数功能

  music停止接口

- 函数定义

  BOOL OPENAT_StopMusic (

   VOID

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  无

### 2. 录音

#### 2.1 OPENAT_StartRecord

- 函数功能

  录音开始接口

- 函数定义

  BOOL OPENAT_StartRecord(

   E_AMOPENAT_RECORD_PARAM *param

   AUD_RECORD_CALLBACK_T cb

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  | 参数  | 注释                                                         |
  | :---: | :----------------------------------------------------------- |
  | param | typedef struct {                                                                                                         char *fileName;//设置保存文件的文件名,使用数据流方式获取数据，该设置无效                                                                                                                                int time_sec;//设置录音的持续时间。时间一到就停止               OpenatRecordMode_t record_mode;//设置数据的获取方式                                  E_AMOPENAT_RECORD_QUALITY quality;//设置录音的质量 E_AMOPENAT_RECORD_TYPE type;//设置录音的类型 E_AMOPENAT_AUD_FORMAT format;//设置数据的格式 AUD_STREAM_RECORD_CALLBACK_T stream_record_cb;//设置数据流回调函数 }E_AMOPENAT_RECORD_PARAM; |
  |  cb   | 录音回调函数                                                 |

#### 2.2 OPENAT_StopRecord

- 函数功能

  录音停止接口

- 函数定义

  BOOL OPENAT_StopRecord(

   VOID

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  无

### 3. TONE

#### 3.1 OPENAT_PlayTone

- 函数功能

  TONE音播放接口

- 函数定义

   BOOL OPENAT_PlayTone(

   E_AMOPENAT_TONE_TYPE* type,

   UINT16 duration,

   E_AMOPENAT_SPEAKER_GAIN volume

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  |   参数   | 注释                                                         |
  | :------: | :----------------------------------------------------------- |
  |   type   | 播放类型 typedef enum {                                                                    PENAT_AUD_TONE_DIAL,                                             OPENAT_AUD_TONE_BUSY,   OPENAT_AUD_TONE_PATH_ACKNOWLEGEMENT, OPENAT_AUD_TONE_CALL_DROPPED,      OPENAT_AUD_TONE_SPECIAL_INFO,        OPENAT_AUD_TONE_CALL_WAITING,                   OPENAT_AUD_TONE_RINGING,                                    OPENAT_AUD_TONE_END,                                         }E_AMOPENAT_TONE_TYPE; |
  | duration | 播放时长                                                     |
  |  volume  | 播放音量                                                     |

#### 3.2 OPENAT_StopTone

- 函数功能

  tone停止接口

- 函数定义

  BOOL OPENAT_StopTone(

   VOID

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  无

### 4. DTMF

#### 4.1 OPENAT_PlayDtmf

- 函数功能

  dtmf播放接口

- 函数定义

   BOOL OPENAT_PlayTone(

   E_AMOPENAT_DTMF_TYPE* type,

   UINT16 duration,

   E_AMOPENAT_SPEAKER_GAIN volume

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  |   参数   |                             注释                             |
  | :------: | :----------------------------------------------------------: |
  |   type   | 播放类型 typedef enum { OPENAT_AUD_TONE_DTMF_0, OPENAT_AUD_TONE_DTMF_1, OPENAT_AUD_TONE_DTMF_2, OPENAT_AUD_TONE_DTMF_3, OPENAT_AUD_TONE_DTMF_4, OPENAT_AUD_TONE_DTMF_5, OPENAT_AUD_TONE_DTMF_6, OPENAT_AUD_TONE_DTMF_7, OPENAT_AUD_TONE_DTMF_8, OPENAT_AUD_TONE_DTMF_9, OPENAT_AUD_TONE_DTMF_A, OPENAT_AUD_TONE_DTMF_B, OPENAT_AUD_TONE_DTMF_C, OPENAT_AUD_TONE_DTMF_D, OPENAT_AUD_TONE_DTMF_HASH, OPENAT_AUD_TONE_DTMF_STAR, OPENAT_AUD_TONE_DTMF_END }E_AMOPENAT_DTMF_TYPE; |
  | duration |                           播放时长                           |
  |  volume  |                           播放音量                           |

#### 3.2 OPENAT_StopDtmf

- 函数功能

  dtmf停止接口

- 函数定义

  BOOL OPENAT_StopDtmf (

   VOID

   )

- 返回值

  TRUE: 成功

  FALSE: 失败

- 参数描述

  无

## 测试用例

--测试用例1：测试ADC0检测范围为OPENAT_ADC_SCALE_DEFAULT。

​         -步骤1：配置相关参数，设置ADC值为1400，电压值为4200。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC0。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC0。

​         -结果  ：三接口均返回TURE。

--测试用例2：测试ADC0检测范围为OPENAT_ADC_SCALE_1V250。

​         -步骤1：配置相关参数，设置ADC值为300，电压值为900。

​         -步骤2：调用打开ADC接口，打开ADC0。

​		 -步骤3：调用读ADC接口，将相关参数带入。

​         -步骤4：调用关闭ADC接口，关闭ADC0。

​         -结果  ：三接口均返回TURE。

--测试用例3：测试ADC0检测范围为OPENAT_ADC_SCALE_2V444。

​         -步骤1：配置相关参数，设置ADC值为600，电压值为1800。

​         -步骤2：从外部接入1.8V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC0。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC0。

​         -结果  ：三接口均返回TURE。

--测试用例4：测试ADC0检测范围为OPENAT_ADC_SCALE_3V233。

​         -步骤1：配置相关参数，设置ADC值为900，电压值为2700。

​         -步骤2：从外部接入3V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC0。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC0。

​         -结果  ：三接口均返回TURE。

--测试用例5：测试ADC0检测范围为OPENAT_ADC_SCALE_5V000。

​         -步骤1：配置相关参数，设置ADC值为1500，电压值为4500。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC0。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC0。

​         -结果   ：三接口均返回TURE。

--测试用例6：测试ADC1检测范围为OPENAT_ADC_SCALE_DEFAULT。

​         -步骤1：配置相关参数，设置ADC值为1400，电压值为4200。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果  ：三接口均返回TURE。

--测试用例7：测试ADC1检测范围为OPENAT_ADC_SCALE_1V250。

​         -步骤1：配置相关参数，设置ADC值为300，电压值为900。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果  ：三接口均返回TURE。

--测试用例8：测试ADC1检测范围为OPENAT_ADC_SCALE_2V444。

​         -步骤1：配置相关参数，设置ADC值为600，电压值为1800。

​         -步骤2：从外部接入1.8V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果  ：三接口均返回TURE。

--测试用例9：测试ADC1检测范围为OPENAT_ADC_SCALE_3V233。

​         -步骤1：配置相关参数，设置ADC值为900，电压值为2700。

​         -步骤2：从外部接入3V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果  ：三接口均返回TURE。

--测试用例10：测试ADC1检测范围为OPENAT_ADC_SCALE_5V000。

​         -步骤1：配置相关参数，设置ADC值为1500，电压值为4500。

​         -步骤2：从外部接入5V电压到ADC端口。

​         -步骤3：调用打开ADC接口，打开ADC1。

​		 -步骤4：调用读ADC接口，将相关参数带入。

​         -步骤5：调用关闭ADC接口，关闭ADC1。

​         -结果   ：三接口均返回TURE。