#include <stdio.h>

void* fun(int a) {
	printf("%d\n", a);
	printf("%p\n", a);//传入了非指针数据的参数
}

int main() {
	void* (*p)(char*);//参数为char时候输出为0
	char num;
	p = fun;
	p(256);
	printf("\n");
	p(&num);
	return 0;
}
