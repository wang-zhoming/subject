/************************
* 验证利用sizeof（int）定义数组长度
* 
************/

#include <stdio.h>

int main() {
	char arry1[sizeof(int)] = "110";
	char arry2[sizeof(long)] = "1100";
	printf("%d\n", sizeof(int));
	printf("%d\n", sizeof(long));
	return 0;
}
