/*****************************
* 功能：计算一个英文句子中最长单词的长度（字母个数）max
* 说明：句子只有字母和空格，空格之间连续字母为单词，句子以'.'结束
* 
******************/

#include <stdio.h>

int main(){
	static char ss[]="Welcome to HeZhou", * ts;
	//gets(ss);
	int maxs = 0, lengths = 0;
	ts = ss;
	while (*ts != '.') {
		while (((*ts <= 'z') && (*ts >= 'a')) || ((*ts <= 'Z') && (*ts >= 'A'))) {
			lengths++;
			ts++;
		}
		if (maxs < lengths)maxs = lengths;//该处代码补全
		lengths = 0;
		ts++;
	}
	printf("The max is:%d\n", maxs);
	return 0;
}






